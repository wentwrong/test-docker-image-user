fixture `Failed test`;

test(`it should fail on google`, async t => {
    await t.navigateTo('https://google.com/')
    await t.expect(false).ok();
});

test(`it should fail on gitlab`, async t => {
    await t.navigateTo('https://gitlab.com/')
    await t.expect(false).ok();
});

